INSTALLATION OPENSHIFT
Change hostname server: example -> okd.example.com
Edit /etc/hosts -> okd.example.com 192.168.122.174
Run script.
Disable firewalld or change iptables (systemctl disable firewalld)
Deploy openshift - oc cluster up --routing-suffix=192.168.122.140 --public-hostname=192.168.122.140
    Here set public ip in routing-suffix and in public-hostname (recommended set domain)
Make login in console (terminal) to check if it's running well: oc login -u system:admin
Connect via web: https://192.168.122.174:8443/console (that's an example)

    Second option to create the cluster:
    - mkdir -p "$HOME/.occluster"
    - oc cluster up --base-dir="$HOME/.occluster"

IAAS/PAAS/SAAS
SaaS: Software as a Service - Software as a Service, also known as cloud application services, represents the most commonly utilized option for businesses in the cloud market
    example: aws, gcloud..
PaaS: Platform as a Service - Cloud platform services, also known as Platform as a Service (PaaS), provide cloud components to certain software while being used mainly for applications
    exaple: openshift (redhat), heroku, google engine
IaaS: Infrastructure as a Service - Cloud infrastructure services, known as Infrastructure as a Service (IaaS), are made of highly scalable and automated compute resources

OPENSHIFT COMPONENTS:
- Etcd: Distributed configuration service, used by kubernetes, to save state information of the recourses and containers inside kubernetes cluster
- Master: Server that orchestate how it works the kubernetes cluster, manage the HA, control how it work kubernetes nodes
- Slave or Minion: Where the containers are running inside kubernetes cluster
- Tag: Used by geographic location, by work group, etc
- Node: make different task, is controlled by Master
- Pod: Group of one or more containers implemented by a node. All this container's pod share the IP address, IPC, hostname and another resources
- Replication controller: Control how many pod's copies make
- Service: Separate pods task definitions. The pod proxies services from kubernetes send automatically service request to the correct pod
- Kubelet: This service execute nodes and read the container manifest and guarantee that container are running and started well
- Kubectl: Config tool in command line from kubernetes

COMPONENTS ADDED BY OPENSHIFT:
- Builds and image streams allow you to build working images and react to new images
- Deployments add expanded supports for the software development and deployment lifecycle
- Routes announce your service to the world

KUBERNETES COMPONENTS:
- Etcd: It stores the configuration information which can be used by each of the nodes in the cluster
- API Server: Kubernetes is an API server which provides all the operation on cluster using the API
- Controller manager: This component is responsible for most of the collectors that regulates the state of cluster and performs a task
- Scheduler: This is one of the key components of Kubernetes master. It is a service in master responsible for distributing the workload

Kubernetes - Node components
- Docker: The first requirement of each node is Docker which helps in running the encapsulated application containers in a relatively isolated but lightweight operating environment
- Kubelet service: This is a small service in each node responsible for relaying information to and from control plane service. It interacts with etcd store to read configuration details and wright values
- Kubernet proxy service: This is a proxy service which runs on each node and helps in making services available to the external host

KOMPOSE TOOL
We can install the tool "Kompose" this tool convert docker-compose file to kubernetes deployment
To convert the file: kompose convert

OPENSHIFT INTRODUCTION:
oc whoami - openshift user
oc login -u developer -p developer - make login
oc get pods -n namespace - get pods
oc get projects - show projects
oc project mysql-test - go to the project "mysql-test"
oc new-project name_project - create project
oc new-app app - create app
oc get po -w - show the container creation process
oc logs -f name_pod - show logs in real time
oc get events - cluster events
oc adm diagnostics - show diagnostics openshift



--
[root@okd ~]# oc login -u developer -p developer
Login successful.

You have one project on this server: "myproject"

Using project "myproject".
[root@okd ~]# 

[root@okd ~]# oc project mysql-test
Now using project "mysql-test" on server "https://192.168.122.174:8443".
[root@okd ~]# 

[root@okd ~]# oc new-project demo
Now using project "demo" on server "https://192.168.122.174:8443".

[root@okd ~]# oc new-app httpd
--> Found image 207a767 (2 months old) in image stream "openshift/httpd" under tag "2.4" for "httpd"

    Apache httpd 2.4 
    ---------------- 
[root@okd ~]# oc get po -w
NAME             READY     STATUS              RESTARTS   AGE
httpd-1-deploy   1/1       Running             0          10s
httpd-1-nnrcm    0/1       ContainerCreating   0          7s
httpd-1-nnrcm   1/1       Running   0         1m
httpd-1-deploy   0/1       Completed   0         1m
httpd-1-deploy   0/1       Terminating   0         1m
httpd-1-deploy   0/1       Terminating   0         1m

[root@okd ~]# oc get pods
NAME            READY     STATUS    RESTARTS   AGE
httpd-1-nnrcm   1/1       Running   0          2m

[root@okd ~]# oc describe po httpd-1-nnrcm
Name:               httpd-1-nnrcm
Namespace:          demo
Priority:           0
PriorityClassName:  <none>
Node:               localhost/192.168.122.174
Start Time:         Tue, 17 Mar 2020 12:21:06 -0400
Labels:             app=httpd
                    deployment=httpd-1
                    deploymentconfig=httpd
Annotations:        openshift.io/deployment-config.latest-version=1
                    openshift.io/deployment-config.name=httpd
                    openshift.io/deployment.name=httpd-1
                    openshift.io/generated-by=OpenShiftNewApp
                    openshift.io/scc=restricted
Status:             Running
IP:                 172.17.0.12
Controlled By:      ReplicationController/httpd-1

# oc adm diagnostics
[Note] Determining if client configuration exists for client/cluster diagnostics
Info:  Successfully read a client config file at '/root/.kube/config'
Info:  Using context for cluster-admin access: 'demo/192-168-122-174:8443/system:admin'

[Note] Running diagnostic: ConfigContexts[myproject/192-168-122-174:8443/system:admin]
       Description: Validate client config context is complete and has connectivity


--

What is a openshif service? A kubernetes service serves as an internal load balancer. It identifies a set of replicated pods in order to proxy the connections, it recieves to them
    Like pods, services are REST objects.

When we create a pod and we want to expose to internet -> It's required to create a service named "NodePort"

[root@okd ~]# oc new-app centos/mysql-57-centos7 -e 'MYSQL_ROOT_PASSWORD=master'

[root@okd ~]# oc get pods -w
NAME                        READY     STATUS              RESTARTS   AGE
mysql-57-centos7-1-deploy   0/1       ContainerCreating   0          2s
mysql-57-centos7-1-6wcsv   0/1       Pending   0         0s
mysql-57-centos7-1-6wcsv   0/1       Pending   0         0s
mysql-57-centos7-1-6wcsv   0/1       ContainerCreating   0         0s
mysql-57-centos7-1-deploy   1/1       Running   0         3s

another example -->


[root@okd ~]# oc new-app --docker-image=docker.io/mariadb --name mariadb -e 'MYSQL_USER=cristian' -e 'MYSQL_PASSWORD=master' -e 'MYSQL_DATABASE=prod    uction' -e 'MYSQL_ROOT_PASSWORD=master'

[root@okd ~]# oc get pods -w
NAME               READY     STATUS              RESTARTS   AGE
mariadb-1-bsp5j    0/1       ContainerCreating   0          3s
mariadb-1-deploy   1/1       Running             0          5s


^C[root@okd ~]# oc get svc
NAME      TYPE        CLUSTER-IP      EXTERNAL-IP   PORT(S)    AGE
mariadb   ClusterIP   172.30.76.126   <none>        3306/TCP   1m
[root@okd ~]# oc descrive svc mariadb
Error: unknown command "descrive" for "oc"

Did you mean this?
	describe

Run 'oc --help' for usage.
[root@okd ~]# oc describe svc mariadb
Name:              mariadb
Namespace:         nodeport2
Labels:            app=mariadb
Annotations:       openshift.io/generated-by=OpenShiftNewApp
Selector:          app=mariadb,deploymentconfig=mariadb
Type:              ClusterIP
IP:                172.30.76.126
Port:              3306-tcp  3306/TCP
TargetPort:        3306/TCP
Endpoints:         <none>
Session Affinity:  None
Events:            <none>


To show the service: oc get svc
oc edit svc mariadb -->

creationTimestamp: 2020-02-01T06:17:56Z
 labels:
 app: mariadb
 name: mariadb
 namespace: nodeport2
 resourceVersion: "186804"
 selfLink: /api/v1/namespaces/nodeport2/services/mariadb
 uid: 968dc3a3-44ba-11ea-b1ed-08002771c7bd
spec:
 clusterIP: 172.30.68.169
 externalTrafficPolicy: Cluster
 ports:
 - name: 3306-tcp
 nodePort: 30306 --> we have to edit this line ----
 port: 3306
 protocol: TCP
 targetPort: 3306
 nodePort: 30306
 selector:
 app: mariadb
 deploymentconfig: mariadb
 sessionAffinity: None
 type: NodePort --> we have to edit this line ----
status:
 loadBalancer: {}

 oc describe svc mariadb
Name: mariadb
Namespace: nodeport2
Labels: app=mariadb
Annotations: openshift.io/generated-by=OpenShiftNewApp
Selector: app=mariadb,deploymentconfig=mariadb
Type: ClusterIP
IP: 172.30.68.169
Port: 3306-tcp 3306/TCP
TargetPort: 3306/TCP
Endpoints: 172.17.0.17:3306
Session Affinity: None
 Events: <none>


#mysql -u cristian -h 192.168.122.174 -P 30306 -p
Enter password: 
Welcome to the MySQL monitor.  Commands end with ; or \g.
Your MySQL connection id is 9
Server version: 5.5.5-10.4.12-MariaDB-1:10.4.12+maria~bionic mariadb.org binary distribution

Copyright (c) 2000, 2020, Oracle and/or its affiliates. All rights reserved.

Oracle is a registered trademark of Oracle Corporation and/or its
affiliates. Other names may be trademarks of their respective
owners.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

mysql> show databases;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| production         |
+--------------------+


MANUAL SCALING:

Manual Scaling
In addition to rollbacks, you can exercise fine-grained control over the number of replicas from the web console, or by using the oc scale command. For example, the following command sets the replicas in the deployment configuration frontend to 3.

$ oc scale dc frontend --replicas=3

Assigning Pods to Specific Nodes:
----------------------------------
You can use node selectors in conjunction with labeled nodes to control pod placement.

OpenShift Container Platform administrators can assign labels during an advanced installation, or added to a node after installation.

Cluster administrators can set the default node selector for your project in order to restrict pod placement to specific nodes. As an OpenShift Container Platform developer, you can set a node selector on a pod configuration to restrict nodes even further.

To add a node selector when creating a pod, edit the pod configuration, and add the nodeSelector value. This can be added to a single pod configuration, or in a pod template:

apiVersion: v1
kind: Pod
spec:
  nodeSelector:
    disktype: ssd
...

CREATE POLICY CONSTRAINT:

#oc adm policy add-scc-to-user anyuid -z default --> this rule allow to exec one container as root 

Then we have to deploy de application: oc new-app sameersbn/squid

DEPLOYMENTS ERRORS
kubectl logs name_pod
oc get dc -> show deploy controller configuration

We can set variables to the deploy controller
kubectl set env dc/mysql-55-centos7 'MYSQL_ROOT_PASSWORD=master' -> dc/mysql-55-centos7 es el deploy controller


PODS COMMUNICATION

#oc new-project finish
#oc create -f mysql.json
#oc proces mysql-ephemeral -p 'MYSQL_ROOT_PASSWORD=master' | oc create -f -
#oc get pods 
#oc rsh mysql-pod

OPENSHIFT REGISTRY -s2i

To install s2i run the s2i.sh script 

#oc get is -> image stream


SCALATE PODS
With our project, we go to our console with the webbrowser, project, monitoring, and there we can scale up or down our pods

In terminal:
- #oc get dc
[root@localhost okd]# oc get dc
NAME      REVISION   DESIRED   CURRENT   TRIGGERED BY
ruby-ex   1          1         1         config,image(ruby-ex:latest)

- [root@localhost okd]# oc scale --replicas=3 dc/ruby-ex
-->   deploymentconfig.apps.openshift.io/ruby-ex scaled



show pods: 

[root@localhost okd]# oc get pods -owide
NAME              READY     STATUS      RESTARTS   AGE       IP            NODE        NOMINATED NODE
ruby-ex-1-build   0/1       Completed   0          6h        172.17.0.6    localhost   <none>
ruby-ex-1-cqxk7   1/1       Running     0          2m        172.17.0.10   localhost   <none>
ruby-ex-1-csb2t   1/1       Running     0          6h        172.17.0.8    localhost   <none>
ruby-ex-1-zfkcv   1/1       Running     0          2m        172.17.0.6    localhost   <none>


PERSISTEN VOLUMES
# oc get pvc
# oc get pv

You can create a persistent volume claim (PVC) and assign it to a deploymentConfig
in a single operation using the oc volume. Assuming you have created an application
called “cotd” inside project “myproject”, then you would issue an instruction similar
to below. Replace $VOLUMECLAIMNAME, $VOLUMECLAIMSIZE, $MOUNTPATH, and $VOLUME
NAME to reflect your environment. Sample settings could be myvolumeclaim, 100Mi,
and /opt/app-root/src/data. Note that in this instance, the $MOUNTPATH denotes the
path inside your container:


$ oc volume dc/cotd --add \
 --name=images
Working with Storage | 15
 --type=persistentVolumeClaim \
 --mount-path=/opt/app-root/src/data/images \
 --claim-name=$VOLUMECLAIMNAME \
 --claim-size=$VOLUMECLAIMSIZE \
 --mount-path=$MOUNTPATH \
 --containers=cotd \
 --overwrite


EXPOSE Service
# oc get svc
# oc expose svc apache-php
# oc get route

IMAGE stream
# oc get is

SET ENVIRONMENT VARIABLES TO A DC / DEPLOY CONTROLLER
# oc set env dc/final-php 'MYSQL_PASS=master'
# oc get pod -w


EXERCISE -> https://github.com/moshinde/Kube-nodejs-mysql



IMPORTANT:

When we have an error when we deploy a pod, normally we have to run before the security CONSTRAINT
# oc new-project app_testing

# oc adm policy add-scc-to-user anyuid -z default

# oc new-app crisjk93/apache2

# oc get pods -w

# oc get pods -owide

Make a curl: while true; do curl -I welcome-welcome.2886795309-80-host05nc.environments.katacoda.com| head -n 1; done


